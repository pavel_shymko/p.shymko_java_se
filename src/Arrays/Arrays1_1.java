package Arrays;

//Создать и вывести на экран матрицу 2 x 3, заполненную случайными числами из [0, 9]
public class Arrays1_1 {
    public static void main(String[] args) {
        int numbers[][] = new int[2][3];
        for (int i = 0; i < numbers.length; i++) { //строка
            for (int j = 0; j < numbers[i].length; j++) {  //столбец
                numbers[i][j] = (int) (Math.random() * 10);
                System.out.print(numbers[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
