package Arrays;

import java.util.Scanner;

//Найдите сумму наибольшего и наименьшего элементов массива.
public class Arrays1_2 {
    public static void main(String[] args) {

        System.out.println("Введите размер массива: ");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();


        int numbers[] = new int[a];

        int max = numbers[0];


        for (int i = 0; i < numbers.length; i++) {

            numbers[i] = (int) (Math.random() * 100 + 1);
            System.out.print(numbers[i] + "\t");

            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println();

        int min = numbers[0];
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + "\t");

            if (numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println();
        System.out.println("Максимальное число в массиве равно: " + max);
        System.out.println("Минимальное число в массиве равно: " + min);
        int diff = max + min;
        System.out.println("Сумма наибольшего и наименьшего элементов массива = " + diff);
    }
}
