package Arrays;

//Сложить две матрицы одинакового размера.
public class Arrays1_3 {
    public static void main(String[] args) {
        System.out.println("Матрица N1");
        int a[][] = new int[3][5];
        int b[][] = new int[3][5];
        int c[][] = new int[3][5];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = (int) (Math.random() * 10 + 1);
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("\n"+"Матрица N2 ");

        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[i].length; j++) {
                b[i][j] = (int) (Math.random() * 10 + 1);
                System.out.print(b[i][j] + "\t");
            }
            System.out.println();

        }
        System.out.println("\n"+"Сумма матриц N1 и N2 ");

        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < c[i].length; j++) {
                c[i][j] = a[i][j] + b[i][j];
                System.out.print(c[i][j] + "\t");
            }
            System.out.println();
        }
    }

}
