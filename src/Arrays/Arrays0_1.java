package Arrays;

//Сформировать возрастающий массив из четных чисел
public class Arrays0_1 {
    public static void main(String[] args) {
        int top = 0;
        int numbers[] = new int[20];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = top += 2;
            System.out.print(top + "\t");
        }
    }
}
