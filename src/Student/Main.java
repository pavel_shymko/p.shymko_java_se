package Student;

public class Main {
    public static void main(String[] args) {
        //1.Создайте класс Student, в котором  будут объявлены поля (переменные экземпляра) классa – numberCourse,
        //nameStudent. Создайте второй класс Main, в котором будет создан объект ivanov.  Занесите данные в поля
        //и выведите их.
        System.out.println("Задача 1");
        Student ivanov = new Student();
        ivanov.name = "ivanov";
        ivanov.course = 1;
        System.out.println(ivanov.name + " " + ivanov.course);

        //2.Создайте в нашем классе Student второй объект – petrov. Занесите для него значение в поля. Выведите
        // данные для обоих объектов.
        System.out.println("Задача 2");
        Student petrov = new Student();
        petrov.name = "petrov";
        petrov.course = 2;
        System.out.println(petrov.name + " " + petrov.course);

        //3.Создайте в нашем классе Student объект – petrov просто присвоив ему ivanov. Выведите данные про оба
        // объекта. Затем измените данные через ссылку ivanov и снова выведите данные. Проанализируйте полученные результаты.
        System.out.println("Задача 3");
        petrov = ivanov;
        System.out.println(ivanov.name + " " + ivanov.course);
        System.out.println(petrov.name + " " + petrov.course);
        ivanov.name = "ivanovs";
        ivanov.course = 3;
        System.out.println(ivanov.name + " " + ivanov.course);
        System.out.println(petrov.name + " " + petrov.course);

        //4.Создайте в нашем классе Student метод выводящий параметры студента на консоль (тип возвращаемого
        // значения у метода  void)
        System.out.println("Задача 4");
        ivanov.printInfo();
        petrov.printInfo();

        //5.Добавьте в нашем классе Student метод вычисляющий средний средний бал студента, по введенному массиву оценок.
        //добавлено в классе Sudent
        //6.Создайте в нашем классе Student метод вычисляющий средний бал по переданному в метод массиву оценок
        System.out.println("Задача 5-6");
        petrov = new Student();
        petrov.name = "petrov";
        petrov.course = 4;
        ivanov.marks = new int[]{4, 4, 5, 5};
        petrov.marks = new int[]{4, 3, 3, 5};
        System.out.println(ivanov.name + " " + ivanov.getAverage());
        System.out.println(petrov.name + " " + petrov.getAverage());

        //7.Создайте в нашем классе Student конструктор без параметров, задающий начальные значения для студента.
        System.out.println("Задача 7");
        Student sidorov = new Student();
        System.out.println(sidorov.name + " " + sidorov.course);

        //8.Создайте в нашем классе Student конструктор с параметрами.
        System.out.println("Задача 8");
        Student igor=new Student("igor",3) ;
        igor.printInfo();

    }
}
