package Student;

public class Student {
    //Задачи 1-3
    public String name;
    public int course;
    public int[] marks; //Задача 5

    //КОНСТРУКТОРЫ/Задача 7
    public Student() {
        name = "student";
        course = 1;
    }

    //КОНСТРУКТОРЫ/Задача 8
    public Student(String name, int level) {
this.name=name;
this.course=level;
    }

    //задача 4
    public void printInfo() {
        System.out.println(name + " " + course);
    }

    //Задача 5
    public float getAverage() {
        int sum = 0;
        for (int i = 0; i < marks.length; i++) {
            sum += marks[i];
        }
        return (float) sum / marks.length;


    }

}
