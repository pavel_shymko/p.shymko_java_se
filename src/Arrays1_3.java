//Сложить две матрицы одинакового размера.
public class Arrays1_3 {
    public static void main(String[] args) {
        System.out.println("Матрица N1");
        int A[][] = new int[3][5];
        int B[][] = new int[3][5];
        int C[][] = new int[3][5];

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[i].length; j++) {
                A[i][j] = (int) (Math.random() * 10 + 1);
                System.out.print(A[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("Матрица N2 ");

        for (int i = 0; i < B.length; i++) {
            for (int j = 0; j < B[i].length; j++) {
                B[i][j] = (int) (Math.random() * 10 + 1);
                System.out.print(B[i][j] + "\t");
            }
            System.out.println();

        }
        System.out.println("Матрица N3 ");

        for (int i = 0; i < C.length; i++) {
            for (int j = 0; j < C[1].length; j++) {
                C[i][j] = A[i][j] + B[i][j];
                System.out.print(C[i][j] + "\t");
            }
            System.out.println();
        }
    }

}
