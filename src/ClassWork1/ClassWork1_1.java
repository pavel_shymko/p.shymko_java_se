package ClassWork1;

//В данной строке найти количество цифр.
public class ClassWork1_1 {
    public static void main(String[] args) {
        String s = "Ho1m0e";
        String k = "Ho2m4el5ess6";

        char[] dom = s.toCharArray();
        int a = 0;

        System.out.print("Первое слово: " + s + "\n");
        System.out.println("В первом слове нахордится : " + dom.length + " символов");
        for (int i = 0; i < dom.length; i++) {
            char c = dom[i];
            if (c > 47 && c < 58) {
                a++;
            }
        }
        System.out.println("Среди символов находится " + a + " цифр");
        System.out.println();
        char[] domik = k.toCharArray();
        int b = 0;
        System.out.print("Второе слово: " + k + "\n");
        System.out.println("Во втором слове нахордится : " + domik.length + " символов");
        for (int i = 0; i < domik.length; i++) {
            char d = domik[i];
            if (d > 47 && d < 58) {
                b++;
            }
        }
        System.out.println("Среди символов находится " + b + " цифр");
    }
}
