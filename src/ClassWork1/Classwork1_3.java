package ClassWork1;

//Написать программу получения строки, в которой удалены все «лишние» пробелы,
// то есть из нескольких подряд идущих пробелов оставить только один.
public class Classwork1_3 {
    public static void main(String[] args) {
        String s = "Hel  lo   Worl  d";
        char[] mas = s.toCharArray();
        boolean b = false;
        for (int i = 0; i < mas.length; i++) {
            char c = mas[i];
            if (mas[i] != ' ' || (mas[i + 1]) != ' ') {
                System.out.print(mas[i]);
            }
        }
    }
}
