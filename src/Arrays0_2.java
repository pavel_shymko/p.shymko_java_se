//Найти количество четных чисел в массиве
public class Arrays0_2 {
    public static void main(String[] args) {
        int numbers[] = new int[20];
        int count = 0;
        System.out.println("Массив содержит cледующие числа: ");
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (int) (Math.random() * 100 + 1);
            System.out.print(numbers[i] + "\t");
        }
        System.out.println();
        System.out.println("В массиве содержатся следующие четные числа: ");
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 == 0) {
                System.out.print(numbers[i] + "\t");
                count++;
            }
        }
        System.out.println();
        System.out.println("Всего четных чисел в массиве:" +
                " "+count);
    }
}
