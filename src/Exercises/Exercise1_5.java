package Exercises;

import java.util.Scanner;

public class Exercise1_5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите значение: ");
        int a = scanner.nextInt();
        int i = 1;
        int b;
        while (i > 0) {
            i++;
            if ((b = i * i) / a > 0) {
                System.out.println(b);
                break;
            }
        }
    }
}

