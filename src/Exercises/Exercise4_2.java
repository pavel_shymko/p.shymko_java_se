package Exercises;

import java.util.Scanner;

// Пользователь вводит число, это будет наше количество строи и столбцов(количество строк равно количеству столбцов)
// - например 5, вы должны вывести значения так, что бы по диагонали они уменьшались на единицу:
//9 10 11 12 13
//7   8    9 10 11
//5   6    7   8   9
//3   4    5   6   7
//1   2    3   4   5
public class Exercise4_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество строк");
        int a = scanner.nextInt();
        int c=9;
        for (int i = 0; i < a; i++) {//номер строки
            for (int j = 0; j<a; j++) {
                System.out.print((c + j)+"\t");
            }
            c-=2;
            System.out.println();
        }


    }
}
