package Exercises;

//пользователь вводит количество строк и количество столбцов(на пример 3 4), вы должны вывести
//9 8 7 6
//8 7 6 5
//7 6 5 4
public class Exercise4_1 {
    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            for (int j = 9; j >= 6; j--) {
                System.out.print(j - i);
            }
            System.out.println();
        }
    }
}
