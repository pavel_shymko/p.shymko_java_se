package Exercises;

import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;

public class Exercise2_3 {
    public static void main(String[] args) {
        System.out.println("Введите число: ");
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int sum = 0;

        for (int i = 1; i <= k; i++) {
            sum += i;
            if (sum == k) {
                System.out.println("Такое число 'n' существует");
                System.out.println("Предыдущее значение " + (sum - i));
                System.out.println("Следующее значение " + (sum + i + 1));
                break;
            }
            if (sum / k > 0) {
                System.out.println("Такого числа 'n' нет");
                System.out.println("Ближайшее Число " + sum);
                System.out.println("Предыдущее значение " + (sum - i));
                break;
            }
        }
    }
}
