package Exercises;
//Найдите n-ое число Фибоначчи

import java.util.Scanner;

public class Exercise5_1 {
    private static int fibonachi(int a) {
        if (a == 0) {
            return 0;
        }
        if (a == 1) {
            return 1;
        } else {
            return fibonachi(a - 1) + fibonachi(a - 2);
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите n-ое число в последовательности Фибоначчи");
        int a = scanner.nextInt();
        // получение n-ого по счёту числа Фибоначчи
        int fn = fibonachi(a);
        // вывод n-ого по счёту числа Фибоначчи
        System.out.println(fibonachi(a));
    }
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Введите n-ое число в последовательности Фибоначчи");
//        int a = scanner.nextInt();
//
//        int b = 1;
//        int c = 1;
//        int sum;
//        System.out.print(b + "\t" + c + "\t");
//        for (int i = 3; i <= a; i++) {
//            sum = b + c;
//            System.out.print(sum + "\t");
//            b = c;
//            c = sum;
//
//        }
//    }
}