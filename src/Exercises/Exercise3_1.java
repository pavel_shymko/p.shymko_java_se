package Exercises;

import java.util.Scanner;

public class Exercise3_1 {
    public static void main(String[] args) {
        System.out.println("Введите число: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= i * n; j++) {
                System.out.print(i);
            }
            System.out.println();
        }
    }
}

