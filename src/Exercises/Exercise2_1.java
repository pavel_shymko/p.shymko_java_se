package Exercises;

import java.util.Scanner;

public class Exercise2_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число: ");
        int a = scanner.nextInt();
        if (a >= 2 && a <= 5) {
            System.out.println(a * 10);
        } else if (a > 7 && a < 40) {
            System.out.println(a / 100f);
        } else if (a <= 0 || a > 3000) {
            System.out.println(a * 3);
        } else {
            System.out.println("0");
        }
    }
}
