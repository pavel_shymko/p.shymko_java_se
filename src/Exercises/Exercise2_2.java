package Exercises;

import java.util.Scanner;

public class Exercise2_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a;
        int sum = 0;

        do {
            System.out.println("Введите значение:");
            a = scanner.nextInt();
            sum += a;
        }
        while (a != 0);
        System.out.println(sum);
    }
}
