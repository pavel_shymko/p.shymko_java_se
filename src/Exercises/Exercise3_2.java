package Exercises;

import java.util.Scanner;

public class Exercise3_2 {
    public static void main(String[] args) {
        System.out.println("Введите четырехзначное число: ");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = a / 1000;
        int b1 = a % 1000;
        int c = b1 / 100;
        int c1 = b1 % 100;
        int d = c1 / 10;
        int d1 = c1 % 10;

        if (b == 4 || c == 4 || d == 4 || d1 == 4) {
            System.out.println("Число содержит цифру '4'");
        }
        if (b == 7 || c == 7 || d == 7 || d1 == 7) {
            System.out.println("Число содержит цифру '7'");
        }
    }
}
