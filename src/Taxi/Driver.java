package Taxi;

public class Driver extends Person {

    private int experience;

    Driver(){
        name="ivan";
        surname="ivanov";
        experience=3;
        rate=4.5;
    }

    Driver(String name, String surName, int experience, double rate){
        this.name=name;
        this.surname=surName;
        this.experience=experience;
        this.rate=rate;
   }
   public void displayInfo(){
       System.out.print("Водитель ");
        super.displayInfo();
       System.out.print("  совершил "+ experience+" поездок  "+"\n");
   }


}
