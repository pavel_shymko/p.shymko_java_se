package Taxi;

public class Client extends Person {
    private String homeadress;
    private String workadress;


    public Client() {
        name = "diana";
        surname = "dianova";
        homeadress = "4-12 1st str";
        workadress = "2-3 washington square";
        rate = 4.5;
    }

    public Client(String name, String surname, String homeadress, String workadress, int rate) {

        this.name = name;
        this.surname = surname;
        this.homeadress = homeadress;
        this.workadress = workadress;
        this.rate = rate;


    }

    public void displayInfo(){
        System.out.print("Клиент  ");
super.displayInfo();
        System.out.print(" совершил "+(int)( Math.random()*10+1)+" поездок "+"\n");
        System.out.println("Пункт назначениия клиента: "+homeadress);
    }
}
