package Taxi;

public class Car {
    public String brand;
    public String model;
    public int year;
    public String klass;

    //Конструктор
    public Car() {
        brand = "renault";
        model = "logan";
        year = 2018;
        klass = "a";
    }

    //Конструктор
    public Car(String brand, String model, int year, String klass) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.klass = klass;
    }
    public void displayCarInfo() {
        System.out.println("Ваш автомобиль  " + brand + " " + model + " " + year + " года выпуска, класс автомобиля " + klass);
    }
}