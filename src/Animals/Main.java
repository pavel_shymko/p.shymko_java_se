package Animals;

import org.w3c.dom.ls.LSOutput;

public class Main {
    public static void main(String[] args) {

        //.  Создайте несколько объектов класса
        Animals cat = new Animals();
        cat.name = "Barsik";
        cat.age = 3;
        cat.weight = 8;
        cat.height = 18;

        Animals horse = new Animals();

        Animals dog = new Animals("Bobik", 5, 12, 60);


//Напишите конструктор без параметров и конструктор с параметрами. Напишите метод, выводящий все данные о животном на консоль.
        dog.printInfo();
        cat.printInfo();
        horse.printInfo();

//Метод, определяющий одинаковые ли имена у двух животных
        System.out.print("\n"+"Мы сравниваем имя собаки: \""+dog.name+ "\" и имя кота \"" +cat.name+ "\" результат сравниения:");
        if ((dog.name.equals(cat.name))==true){
            System.out.print(" ИМЕНА ОДИНАКОВЫЕ");
        }
        else {
            System.out.print(" ИМЕНА РАЗНЫЕ");
        }
//Метод, сравнивающий поля объекта с пороговыми значениями Возраст, Вес, Рост
// (пример вывода: старше 5 лет; легче 2 кг; выше 20 см).
        System.out.println();
        System.out.print("Сравниваемые параметры: ");
        Animals standart=new Animals();
        if ((dog.age-standart.age)>0){
            System.out.print(dog.name+" старше на "+(dog.age-standart.age)+" лет, ");
        }
        else {
            System.out.print(dog.name+" младше на "+(dog.age-standart.age)+" лет, ");
        }
        if ((dog.weight-standart.weight)>0){
            System.out.print("тяжелее на "+(dog.weight-standart.weight)+" кг, ");
        }
        else {
            System.out.print("легче на " + (dog.weight - standart.weight) + " кг, ");
        }
        if ((dog.height-standart.height)>0){
            System.out.print("выше на "+(dog.height-standart.height)+" см.");
        }
        else {
            System.out.print("ниже на " + (dog.height - standart.height) + " см.");
        }

    }
}
