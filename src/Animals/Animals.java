package Animals;

//Напишите класс  Животные, содержаний поля – Имя, Возраст, Вес, Рост. Объявите три константы Возраст, Вес, Рост
// в которые запишите  пороговые значения.
public class Animals {

    public String name;
    public int age = 3;
    public int weight = 5;
    public int height = 7;

    //Напишите конструктор без параметров и конструктор с параметрами. Напишите метод, выводящий все данные о животном на консоль.
    //Напишите конструктор без параметров
    public Animals() {
        name = "horse";
        age = 4;
        weight = 6;
        height = 8;
    }

    //Напишите конструктор с параметрами
    public Animals(String name, int age, int weight, int height) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.height = height;
    }

    // Напишите метод, выводящий все данные о животном на консоль.
    public void printInfo() {
        System.out.println(name + " " + age + " " + weight + " " + height);
    }

//public void animalsName(){
//    char[] mas = name.toCharArray();
//   for (int i = 0; i < mas.length; i++) {
//        char c=mas[i];
//
//        }
//}
//public void Equials(){
//    System.out.println(name.equals(name));
//}
}
